# Internal Session

This library include :

- [spring-library-common](https://gitlab.com/spring-library/common)
- [spring-library-sleuth](https://gitlab.com/spring-library/sleuth)

Internal Session Library is helper to get user session from communication between backend service.

## Setup

To use this library, setup our pom.xml

```xml
<dependency>
    <groupId>com.gitlab.residwi</groupId>
    <artifactId>spring-library-internal-session</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/12070658/-/packages/maven</url>
    </repository>
</repositories>
```

## Internal Session

Internal Session is represented by `InternalSession` class. There are some information on that class, like userId, userName and roles.

```java
public class InternalSession {

    private String userId;

    private String userName;

    private List<String> roles;
}
```

## Get Internal Session

To get Internal Session on Controller, we can add on method parameter. [InternalSessionArgumentResolver](src/main/java/com/gitlab/residwi/spring/library/internalsession/resolver/InternalSessionArgumentResolver.java) will automatically get the data from HTTP Header

```java
@RestController
public class ExampleController {

    @GetMapping(value = "/backend-internal/your-api", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponsePayload<SomeResponse> member(InternalSession internalSession) {

        String userId = internalSession.getUserId;
        String userName = internalSession.getUserName;
        List<String> roles = internalSession.getRoles;
    }
}
```

## Configuration Properties
We can change Internal Session header key using configuration properties.

```properties
library.internal.session.header.user-id=X-Session-UserId
library.internal.session.header.user-name=X-Session-UserName
library.internal.session.header.roles=X-Session-Roles
```

## Swagger Support

To add Internal Session to swagger, we can use `@InternalSessionAtHeader` annotation in controller method.

```java
@RestController
public class ExampleController {

    @InternalSessionAtHeader
    @GetMapping(value = "/backend-internal/only-member", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponsePayload<MemberData> member(InternalSession internalSession) {
        // do something
    }
}
```

## Sleuth Support

If we are using sleuth, we can also get `InternalSession` from sleuth using [InternalSessionHelper](src/main/java/com/gitlab/residwi/spring/library/internalsession/helper/InternalSessionHelper.java).

```java
@Service
public class ExampleService {

    @Autowired
    private Tracer tracer;

    public InternalSession getInternalSession() {
        return InternalSessionHelper.fromSleuth(tracer.currentSpan().context());
    }
}
```