package com.gitlab.residwi.spring.library.internalsession;

import brave.Tracer;
import com.gitlab.residwi.spring.library.common.exception.CommonErrorException;
import com.gitlab.residwi.spring.library.common.helper.ResponseHelper;
import com.gitlab.residwi.spring.library.common.model.response.ResponsePayload;
import com.gitlab.residwi.spring.library.internalsession.helper.InternalSessionHelper;
import com.gitlab.residwi.spring.library.internalsession.model.InternalSession;
import com.gitlab.residwi.spring.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.spring.library.internalsession.swagger.annotation.InternalSessionAtHeader;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = InternalSessionTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class InternalSessionTest {

    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String ROLE_1 = "role1";
    public static final String ROLE_2 = "role2";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InternalSessionProperties properties;

    @Test
    void testInternalSession() throws Exception {
        mockMvc.perform(get("/internal-session")
                .header(properties.getHeader().getUserId(), USER_ID)
                .header(properties.getHeader().getUserName(), USER_NAME)
                .header(properties.getHeader().getRoles(), ROLE_1 + "," + ROLE_2))
                .andExpect(jsonPath("$.code").value(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.status").value(HttpStatus.OK.name()))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.userName").value(USER_NAME))
                .andExpect(jsonPath("$.data.roles[0]").value(ROLE_1))
                .andExpect(jsonPath("$.data.roles[1]").value(ROLE_2))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void testInternalSessionSleuth() throws Exception {
        mockMvc.perform(get("/internal-session-sleuth")
                .header(properties.getHeader().getUserId(), USER_ID)
                .header(properties.getHeader().getUserName(), USER_NAME)
                .header(properties.getHeader().getRoles(), ROLE_1 + "," + ROLE_2))
                .andExpect(jsonPath("$.code").value(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.status").value(HttpStatus.OK.name()))
                .andExpect(jsonPath("$.data.userId").value(USER_ID))
                .andExpect(jsonPath("$.data.userName").value(USER_NAME))
                .andExpect(jsonPath("$.data.roles[0]").value(ROLE_1))
                .andExpect(jsonPath("$.data.roles[1]").value(ROLE_2))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void testInternalSessionSwagger() throws Exception {
        mockMvc.perform(get("/v3/api-docs"))
                .andExpect(jsonPath("$.paths.['/internal-session'].get.parameters[*].name").value(Matchers.contains("userId", "userName", "roles")))
                .andExpect(jsonPath("$.components.parameters.internalSessionHeaderUserId").isNotEmpty())
                .andExpect(jsonPath("$.components.parameters.internalSessionHeaderUserName").isNotEmpty())
                .andExpect(jsonPath("$.components.parameters.internalSessionHeaderRoles").isNotEmpty())
                .andExpect(status().is2xxSuccessful());
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class TestController {

            @Autowired
            private TestService testService;

            @InternalSessionAtHeader
            @GetMapping(path = "/internal-session", produces = MediaType.APPLICATION_JSON_VALUE)
            public ResponsePayload<InternalSession> internalSession(InternalSession internalSession) {
                return ResponseHelper.ok(internalSession);
            }

            @GetMapping(path = "/internal-session-sleuth", produces = MediaType.APPLICATION_JSON_VALUE)
            public ResponsePayload<InternalSession> internalSessionSleuth(InternalSession internalSession) {
                return ResponseHelper.ok(testService.getInternalSession());
            }
        }

        @Service
        public static class TestService {

            @Autowired
            private Tracer tracer;

            public InternalSession getInternalSession() {
                return InternalSessionHelper.fromSleuth(tracer.currentSpan().context());
            }
        }

        @RestControllerAdvice
        public static class ErrorController implements CommonErrorException, MessageSourceAware {

            private static final Logger log = LoggerFactory.getLogger(ErrorController.class);

            private MessageSource messageSource;

            @Override
            public Logger getLogger() {
                return log;
            }

            @Override
            public MessageSource getMessageSource() {
                return messageSource;
            }

            @Override
            public void setMessageSource(MessageSource messageSource) {
                this.messageSource = messageSource;
            }
        }
    }
}
