package com.gitlab.residwi.spring.library.internalsession.resolver;

import com.gitlab.residwi.spring.library.internalsession.event.InternalSessionEvent;
import com.gitlab.residwi.spring.library.internalsession.helper.InternalSessionHelper;
import com.gitlab.residwi.spring.library.internalsession.model.InternalSession;
import com.gitlab.residwi.spring.library.internalsession.properties.InternalSessionProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

public class InternalSessionArgumentResolver implements HandlerMethodArgumentResolver, ApplicationEventPublisherAware {

    private final InternalSessionProperties properties;

    private ApplicationEventPublisher applicationEventPublisher;

    public InternalSessionArgumentResolver(InternalSessionProperties properties) {
        this.properties = properties;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().isAssignableFrom(InternalSession.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
        var request = (HttpServletRequest) nativeWebRequest.getNativeRequest();
        var internalSession = InternalSessionHelper.fromHeader(request, properties);
        applicationEventPublisher.publishEvent(new InternalSessionEvent(internalSession));
        return internalSession;
    }
}
