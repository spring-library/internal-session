package com.gitlab.residwi.spring.library.internalsession.event;

import com.gitlab.residwi.spring.library.internalsession.model.InternalSession;
import org.springframework.context.ApplicationEvent;

public class InternalSessionEvent extends ApplicationEvent {

    private final InternalSession internalSession;

    public InternalSessionEvent(InternalSession internalSession) {
        super(internalSession);
        this.internalSession = internalSession;
    }

    public InternalSession getInternalSession() {
        return internalSession;
    }
}
