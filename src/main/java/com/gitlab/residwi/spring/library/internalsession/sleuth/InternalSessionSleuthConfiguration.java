package com.gitlab.residwi.spring.library.internalsession.sleuth;

import brave.Tracer;
import com.gitlab.residwi.spring.library.internalsession.InternalSessionAutoConfiguration;
import com.gitlab.residwi.spring.library.sleuth.configuration.SleuthConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass({WebMvcConfigurer.class, SleuthConfiguration.class})
@AutoConfigureAfter({SleuthConfiguration.class, InternalSessionAutoConfiguration.class})
public class InternalSessionSleuthConfiguration {

    @Bean
    public InternalSessionExtraFields internalSessionExtraFields() {
        return new InternalSessionExtraFields();
    }

    @Bean
    public InternalSessionSleuthEventListener internalSessionSleuthEventListener(Tracer tracer) {
        return new InternalSessionSleuthEventListener(tracer);
    }
}

