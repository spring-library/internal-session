package com.gitlab.residwi.spring.library.internalsession.swagger;

import com.gitlab.residwi.spring.library.common.CommonSwaggerAutoConfiguration;
import com.gitlab.residwi.spring.library.internalsession.InternalSessionAutoConfiguration;
import com.gitlab.residwi.spring.library.internalsession.properties.InternalSessionProperties;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(CommonSwaggerAutoConfiguration.class)
@AutoConfigureAfter(InternalSessionAutoConfiguration.class)
public class InternalSessionSwaggerAutoConfiguration {

    @Bean
    public Parameter internalSessionHeaderUserId(InternalSessionProperties properties) {
        return new HeaderParameter()
                .required(true)
                .name(properties.getHeader().getUserId())
                .example(properties.getHeader().getUserId());
    }

    @Bean
    public Parameter internalSessionHeaderUserName(InternalSessionProperties properties) {
        return new HeaderParameter()
                .required(true)
                .name(properties.getHeader().getUserName())
                .example(properties.getHeader().getUserName());
    }

    @Bean
    public Parameter internalSessionHeaderRoles(InternalSessionProperties properties) {
        return new HeaderParameter()
                .required(true)
                .name(properties.getHeader().getRoles())
                .example(properties.getHeader().getRoles());
    }
}
