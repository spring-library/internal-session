package com.gitlab.residwi.spring.library.internalsession.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("library.internal.session")
public class InternalSessionProperties {

    private Header header = new Header();

    public InternalSessionProperties(Header header) {
        this.header = header;
    }

    public InternalSessionProperties() {
    }

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public static class Header {

        private String userId = "Baggage-Session-User-Id";

        private String userName = "Baggage-Session-User-Name";

        private String roles = "Baggage-Session-User-Roles";

        public Header(String userId, String userName, String roles) {
            this.userId = userId;
            this.userName = userName;
            this.roles = roles;
        }

        public Header() {
        }

        public String getUserId() {
            return this.userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getRoles() {
            return this.roles;
        }

        public void setRoles(String roles) {
            this.roles = roles;
        }
    }
}
