package com.gitlab.residwi.spring.library.internalsession.sleuth;

import brave.Tracer;
import com.gitlab.residwi.spring.library.internalsession.event.InternalSessionEvent;
import com.gitlab.residwi.spring.library.internalsession.helper.InternalSessionHelper;
import org.springframework.context.ApplicationListener;

public class InternalSessionSleuthEventListener implements ApplicationListener<InternalSessionEvent> {

    private final Tracer tracer;

    public InternalSessionSleuthEventListener(Tracer tracer) {
        this.tracer = tracer;
    }

    @Override
    public void onApplicationEvent(InternalSessionEvent event) {
        InternalSessionHelper.toSleuth(tracer.currentSpan().context(), event.getInternalSession());
    }
}
