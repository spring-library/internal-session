package com.gitlab.residwi.spring.library.internalsession.model;

import io.swagger.v3.oas.annotations.Hidden;

import java.util.List;

@Hidden
public class InternalSession {

    private String userId;

    private String userName;

    private List<String> roles;

    public InternalSession(String userId, String userName, List<String> roles) {
        this.userId = userId;
        this.userName = userName;
        this.roles = roles;
    }

    public InternalSession() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getRoles() {
        return this.roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
