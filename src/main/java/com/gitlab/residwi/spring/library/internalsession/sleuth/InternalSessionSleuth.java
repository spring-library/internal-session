package com.gitlab.residwi.spring.library.internalsession.sleuth;

public final class InternalSessionSleuth {

    public static final String USER_ID = "Session-User-Id";
    public static final String USER_NAME = "Session-User-Name";
    public static final String ROLES = "Session-User-Roles";

    private InternalSessionSleuth() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }
}
