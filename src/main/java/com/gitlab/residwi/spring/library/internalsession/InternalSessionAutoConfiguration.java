package com.gitlab.residwi.spring.library.internalsession;

import com.gitlab.residwi.spring.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.spring.library.internalsession.resolver.InternalSessionArgumentResolver;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@EnableConfigurationProperties(InternalSessionProperties.class)
public class InternalSessionAutoConfiguration implements WebMvcConfigurer {

    private final InternalSessionProperties internalSessionProperties;

    public InternalSessionAutoConfiguration(InternalSessionProperties internalSessionProperties) {
        this.internalSessionProperties = internalSessionProperties;
    }

    @Bean
    public InternalSessionArgumentResolver internalSessionArgumentResolver() {
        return new InternalSessionArgumentResolver(internalSessionProperties);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(internalSessionArgumentResolver());
    }
}
