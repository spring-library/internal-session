package com.gitlab.residwi.spring.library.internalsession.helper;

import brave.baggage.BaggageField;
import brave.propagation.TraceContext;
import com.gitlab.residwi.spring.library.internalsession.model.InternalSession;
import com.gitlab.residwi.spring.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.spring.library.internalsession.sleuth.InternalSessionSleuth;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

public final class InternalSessionHelper {

    private InternalSessionHelper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static InternalSession fromHeader(HttpServletRequest request, InternalSessionProperties properties) {
        var internalSession = new InternalSession();
        internalSession.setUserId(request.getHeader(properties.getHeader().getUserId()));
        internalSession.setUserName(request.getHeader(properties.getHeader().getUserName()));
        internalSession.setRoles(toListOfRoles(request.getHeader(properties.getHeader().getRoles())));

        return internalSession;
    }

    public static InternalSession fromSleuth(TraceContext traceContext) {
        var internalSession = new InternalSession();
        internalSession.setUserId(BaggageField.getByName(traceContext, InternalSessionSleuth.USER_ID).getValue(traceContext));
        internalSession.setUserName(BaggageField.getByName(traceContext, InternalSessionSleuth.USER_NAME).getValue(traceContext));
        internalSession.setRoles(toListOfRoles(BaggageField.getByName(traceContext, InternalSessionSleuth.ROLES).getValue(traceContext)));

        return internalSession;
    }

    public static InternalSession toSleuth(TraceContext traceContext, InternalSession internalSession) {
        BaggageField.getByName(traceContext, InternalSessionSleuth.USER_ID).updateValue(traceContext, internalSession.getUserId());
        BaggageField.getByName(traceContext, InternalSessionSleuth.USER_NAME).updateValue(traceContext, internalSession.getUserName());
        BaggageField.getByName(traceContext, InternalSessionSleuth.ROLES).updateValue(traceContext, toStringOfRoles(internalSession.getRoles()));

        return internalSession;
    }

    private static List<String> toListOfRoles(String roles) {
        if (StringUtils.hasText(roles)) {
            return Arrays.asList(roles.split(","));
        } else {
            return Collections.emptyList();
        }
    }

    private static String toStringOfRoles(List<String> roles) {
        var stringJoiner = new StringJoiner(",");
        roles.forEach(stringJoiner::add);
        return stringJoiner.toString();
    }
}
